package grouptwodeliveryapp.com.deliveryapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The DBHelper class. Handles all of the database queries and so fourth.
 * Created by Benjamin H on 15-11-10.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_ORDERS = "DatabaseOrders";
    private static final int DB_VERSION = 1;
    private static final String PREF_KEY = "Prefs";

    public static final String ID_KEY = "_id";
    public static final String UNDELIVERED_ORDERS_TABLE = "UndeliveredOrders";
    public static final String CUSTOMER_NUMBER_KEY = "CustomerNumber";
    public static final String DELIVERY_ADDRESS_KEY = "DeliveryAddress";
    public static final String ORDER_NUMBER_KEY = "OrderNumber";
    public static final String ORDER_SUM_KEY = "OrderSum";
    public static final String INCOMING_ORDER_DATE_KEY = "IncomingOrderDate";
    public static final String ORDER_DATE_KEY = "IncomingOrderDate";

    public static final String DELIVERED_ORDERS_TABLE = "DeliveredOrders";
    public static final String DELIVERY_DATE_KEY = "DeliveryDate";
    public static final String TIME_STAMP_KEY = "TimeStamp";
    public static final String GPS_LATITUDE_KEY = "Latitude" ;
    public static final String GPS_LONGITUDE_KEY = "Longitude";

    private Context context;
    private SQLiteDatabase db;
    private SimpleDateFormat simpleDateFormat;
    private Date date;

    public static String deliveryDate;
    public static String timeStamp;

    public DBHelper(Context context) {
        super(context, DATABASE_ORDERS, null, DB_VERSION);
        this.context = context;
    }

    /**
     * Creating the database and the two Tables UndeliveredOrders
     * and DeliveredOrders. This method (onCreate) will only be called
     * if the Database does not exist. So in this case when the Database is
     * already created this code will NOT execute.
     * Leaving this code intact in case of future problems.
     * @param db The name of the Database variable.
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlUdo = "CREATE TABLE " + UNDELIVERED_ORDERS_TABLE + " (";
        sqlUdo += ID_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        sqlUdo += CUSTOMER_NUMBER_KEY + " VARCHAR(255),";
        sqlUdo += DELIVERY_ADDRESS_KEY + " VARCHAR(255),";
        sqlUdo += ORDER_NUMBER_KEY + " VARCHAR(255),";
        sqlUdo += ORDER_SUM_KEY + " VARCHAR(255),";
        sqlUdo += INCOMING_ORDER_DATE_KEY + " VARCHAR(255)";
        sqlUdo += " );";

        String sqlDo = "CREATE TABLE " + DELIVERED_ORDERS_TABLE + " (";
        sqlDo += ID_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        sqlDo += DELIVERY_DATE_KEY + " VARCHAR(255),";
        sqlDo += TIME_STAMP_KEY + " VARCHAR(255),";
        sqlDo += CUSTOMER_NUMBER_KEY + " VARCHAR(255),";
        sqlDo += DELIVERY_ADDRESS_KEY + " VARCHAR(255),";
        sqlDo += ORDER_NUMBER_KEY + " VARCHAR(255),";
        sqlDo += ORDER_SUM_KEY + " VARCHAR(255),";
        sqlDo += GPS_LATITUDE_KEY + " VARCHAR(255),";
        sqlDo += GPS_LONGITUDE_KEY + " VARCHAR(255)";
        sqlDo += " );";

        /**
         * Executes the SQL commands stored in the two Strings sqlUdo and sqlDo
         * into the variable db which is the variable connected to the Database.
         */
        db.execSQL(sqlUdo);
        db.execSQL(sqlDo);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * Add random orders to the database with help from the class GenerateOrder. The number of orders
     * that will be generated is determined by the "sh" variable numOfOrKey.
     */
    public void addRandomOrdersToDatabase() {
        SharedPreferences sh = context.getSharedPreferences(PREF_KEY, context.MODE_PRIVATE);

        int numberOfOrders = sh.getInt("numOfOrKey", 10);

        for(int i = 0; i < numberOfOrders; i++) {
            simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
            date = new Date();

            db = getWritableDatabase();
            GenerateOrder go = new GenerateOrder(context);
            go.newOrder();
            
            ContentValues cvs = new ContentValues();
            cvs.put(CUSTOMER_NUMBER_KEY, go.getCostumernr());
            cvs.put(DELIVERY_ADDRESS_KEY, go.getAddress());
            cvs.put(ORDER_NUMBER_KEY, go.getOrdernr());
            cvs.put(ORDER_SUM_KEY, go.getSum());
            cvs.put(ORDER_DATE_KEY, (simpleDateFormat.format(date)));

            db.insert(UNDELIVERED_ORDERS_TABLE, null, cvs);
            closeDatabase();
        }
    }

    /**
     * Closing the database.
     */
    public void closeDatabase() {
        db.close();
    }

    /**
     * Saves a specific order to the database when you hit the delivery button inside
     * the class SettingsActivity.
     * @param rowPosition _id position.
     * @param location Stores the location where you are.
     */
    public void saveUndeliveredOrderToDeliveredOrders(long rowPosition, Location location) {
        db = getWritableDatabase();
        simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");
        date = new Date();

        deliveryDate = simpleDateFormat.format(date);
        timeStamp = DateFormat.getTimeInstance().format(date);

        String latitudeStr = String.valueOf(location.getLatitude());
        String longitudeStr = String.valueOf(location.getLongitude());

        String customerNumberStr = "";
        String deliveryAddressStr = "";
        String orderNumberStr = "";
        String orderSumStr = "";

        Cursor cursor = db.rawQuery("SELECT * FROM " + UNDELIVERED_ORDERS_TABLE +
                " WHERE " + ID_KEY + " = " + rowPosition + ";", null);

        if (cursor.moveToFirst()) {
            customerNumberStr = cursor.getString(cursor.getColumnIndex(CUSTOMER_NUMBER_KEY));
            deliveryAddressStr = cursor.getString(cursor.getColumnIndex(DELIVERY_ADDRESS_KEY));
            orderNumberStr = cursor.getString(cursor.getColumnIndex(ORDER_NUMBER_KEY));
            orderSumStr = cursor.getString(cursor.getColumnIndex(ORDER_SUM_KEY));
        }

        ContentValues cvs = new ContentValues();

        cvs.put(DELIVERY_DATE_KEY, deliveryDate);
        cvs.put(TIME_STAMP_KEY, timeStamp);
        cvs.put(CUSTOMER_NUMBER_KEY, customerNumberStr);
        cvs.put(DELIVERY_ADDRESS_KEY, deliveryAddressStr);
        cvs.put(ORDER_NUMBER_KEY, orderNumberStr);
        cvs.put(ORDER_SUM_KEY, orderSumStr);
        cvs.put(GPS_LATITUDE_KEY, latitudeStr);
        cvs.put(GPS_LONGITUDE_KEY, longitudeStr);

        db.insert(DELIVERED_ORDERS_TABLE, null, cvs);

        closeDatabase();
    }

    /**
     * Getting all the undelivered orders from the database.
     * @return Everything that is stored in the table UndeliveredOrders.
     */
    public Cursor getUndeliveredOrdersFromDatabase() {
        db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + UNDELIVERED_ORDERS_TABLE + ";", null);
    }

    /**
     * Getting all the delivered orders from the database.
     * @return Everything that is stored in the table DeliveredOrders.
     */
    public Cursor getDeliveredOrdersFromDatabase() {
        db = getReadableDatabase();
        return db.rawQuery("SELECT * FROM " + DELIVERED_ORDERS_TABLE + ";", null);
    }

    /**
     * Clear all of the undelivered orders from the table UndeliveredOrders.
     */
    public void clearTableFromUndeliveredOrders() {
        db = getWritableDatabase();
        db.execSQL("DELETE FROM UndeliveredOrders");
        closeDatabase();
    }

    /**
     * Clear all of the delivered orders from the table DeliveredOrders.
     */
    public void clearTableFromDeliveredOrders() {
        db = getWritableDatabase();
        db.execSQL("DELETE FROM DeliveredOrders");
        closeDatabase();
    }

    /**
     * Delete the order from the table UndeliveredOrders when the undelivered order
     * gets delivered.
     * @param id Which _id position row that would be deleted.
     */
    public void deleteUndeliveredOrderFromDataBase(long id) {
        db = getWritableDatabase();

        String idStr = String.valueOf(id);

        String selection = "_id=?";
        String[] selectionArgs = {idStr};

        db.delete(UNDELIVERED_ORDERS_TABLE, selection, selectionArgs);
        closeDatabase();
    }
}
