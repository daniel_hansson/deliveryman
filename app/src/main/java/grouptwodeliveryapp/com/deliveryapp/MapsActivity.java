package grouptwodeliveryapp.com.deliveryapp;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private double lat;
    private double lng;
    private String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Intent intent = getIntent();
        lat = intent.getDoubleExtra(OrderDetailActivity.LATITUDE_ORDER_DETAILS, 0);
        lng = intent.getDoubleExtra(OrderDetailActivity.LONGITUDE_ORDER_DETAILS, 0);
        message = intent.getStringExtra(OrderDetailActivity.MESSAGE_ORDER_DETAILS);
    }

    /**
     * Sets map coordinates to lat/lng from intent.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng coordinates = new LatLng(lat, lng);
        mMap.addMarker(new MarkerOptions().position(coordinates).title(message));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 15));
    }
}
