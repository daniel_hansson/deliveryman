package grouptwodeliveryapp.com.deliveryapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

public class SettingsActivity extends AppCompatActivity {
    public static final String PREF_KEY = "Prefs";
    private SeekBar seekBar;
    private TextView showNumberOfOrders;
    private TextView phoneNumberTextField;
    private int numberOfOrders;
    private String phoneNumber;
    private boolean delivered;
    private SharedPreferences sh;
    private Toolbar myToolbar;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setUpVariables();

        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        showNumbersOnSeekBar();

        intent = getIntent();
        delivered = intent.getBooleanExtra(OrderListActivity.DELIVERED_VALUE, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                if(intent.getBooleanExtra(OrderDetailActivity.FROM_ORDER_DETAILS, false)){
                    finish();
                }else {
                    startOrderListActivity(delivered);
                }
                return true;
            case R.id.undelivered_menu:
                startOrderListActivity(false);
                return true;
            case R.id.delivered_menu:
                startOrderListActivity(true);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startOrderListActivity(boolean delivered){
        Intent intent = new Intent(this, OrderListActivity.class);
        intent.putExtra(OrderListActivity.DELIVERED_VALUE, delivered);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void setUpVariables() {
        myToolbar = (Toolbar) findViewById(R.id.bar_tool);
        sh = getSharedPreferences(PREF_KEY, MODE_PRIVATE);
        numberOfOrders = sh.getInt("numOfOrKey", 10);

        phoneNumberTextField = (TextView) findViewById(R.id.tele_number);
        seekBar = (SeekBar) findViewById(R.id.number_of_orders);
        showNumberOfOrders = (TextView) findViewById(R.id.show_number_of_orders);
    }

    private void showNumbersOnSeekBar() {

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            /**
             * Showing orders between ten and twenty on the SeekBar.
             * @param seekBar
             * @param progress
             * @param fromUser
             */
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                showNumberOfOrders.setText(" " + (progress + 10) + "/" + (seekBar.getMax() + 10));
                numberOfOrders = (progress + 10);
            }

            /**
             * Alter the color on the number of orders text when moving
             * the SeekBar to green.
             * @param seekBar
             */
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                showNumberOfOrders.setTextColor(getResources().getColor(R.color.seekBarInProgressColor));
            }

            /**
             * Switches back to the default TextView color when stopped scrolling.
             * @param seekBar
             */
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                showNumberOfOrders.setTextColor((getResources().getColor(R.color.defaultSeekBarColor)));
            }
        });
    }

    /**
     * Will save the number of orders that will be generated at a time
     * and the telephone number as SharedPreferences so it will be remembered
     * the next time the app gets started.
     * @param view
     */
    public void saveSettingsButton(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        boolean doThis = true;

        phoneNumber = sh.getString("phoneNumKey", null);
        String phoneNumberStr = phoneNumberTextField.getText().toString();

        if(showNumberOfOrders.getText().toString().equals(" 10/20")) numberOfOrders = 10;

        if((phoneNumber == null) && (phoneNumberStr.isEmpty())) {
            doThis = false;
            builder.setMessage(R.string.settings_alert_text)
                    .setTitle(R.string.settings_alert_title)
                    .setCancelable(false)
                    .setPositiveButton(R.string.settings_positive_button, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = builder.create();
            alert.show();
        }

        if(doThis) {
            SharedPreferences.Editor editor = sh.edit();
            int oldOrderValue = sh.getInt("numOfOrKey", 10);

            if(!(phoneNumberStr.isEmpty())) editor.putString("phoneNumKey", phoneNumberStr);

            editor.putInt("numOfOrKey", numberOfOrders);
            editor.commit();

            phoneNumber = sh.getString("phoneNumKey", null);

            if(!(phoneNumberStr.isEmpty())) {
                builder.setMessage(getString(R.string.settings_message_text_one) + phoneNumber  + getString(R.string.settings_message_text_two) +
                        getString(R.string.settings_message_text_three) + oldOrderValue + getString(R.string.settings_message_text_four) +
                        numberOfOrders)

                        .setTitle(R.string.settings_message_title)
                        .setCancelable(false)
                        .setPositiveButton(R.string.settings_positive_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();

            } else {
                builder.setMessage(getString(R.string.settings_message_text_one) + phoneNumber + getString(R.string.settings_message_text_five)    +
                        getString(R.string.settings_message_text_six) + oldOrderValue +
                        getString(R.string.settings_message_text_four) + numberOfOrders)

                        .setTitle(R.string.settings_message_title)
                        .setCancelable(false)
                        .setPositiveButton(R.string.settings_positive_button, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert = builder.create();
                alert.show();
            }
        }
    }
}
